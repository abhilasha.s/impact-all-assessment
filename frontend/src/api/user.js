import client from "./client";

const login = (info) => client.post("/user/login", info);
const signup = (info) => client.post("/user/signup", info);
const get = ({ token, userName }) =>
	client.post(
		"/user/get",
		{ userName },
		{
			headers: {
				"access-token": token,
			},
		}
	);

// eslint-disable-next-line
export default { login, signup, get };
