import { create } from "apisauce";

const client = create({
	baseURL: "http://localhost:5000/api/v1",
});

export default client;
