const useApi = (apiFunc) => {
	const request = async (...args) => {
		const response = await apiFunc(...args);

		if (!response.ok) {
			console.log(response.data);
			return response;
		}
		return response;
	};

	return { request };
};

export default useApi;
