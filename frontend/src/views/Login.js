import React from "react";
import { useNavigate } from "react-router-dom";
import { routes } from "../config/routes";
import user from "../api/user";
import useApi from "../hooks/useApi";
import "../assets/css/form.css";

export default function Login() {
	const navigate = useNavigate();
	const loginApi = useApi(user.login);

	const [userName, setUserName] = React.useState("");
	const [password, setPassword] = React.useState("");

	const handleSubmit = async () => {
		if (!userName || !password) alert("Please fill in all fields");
		else {
			const response = await loginApi.request({
				userName,
				password,
			});

			if (response.ok) {
				localStorage.setItem("user", JSON.stringify(response.data));
				navigate(routes.PROFILE);
				alert("Login Successful");
			} else alert(`Error: ${response.data.message}!`);
		}
	};

	return (
		<div className="login-container">
			<div className="login">
				<label>
					<p className="user-name">Username</p>
				</label>
				<input
					type="text"
					placeholder="Enter User Name"
					name="user-name"
					className="login-input"
					required
					onChange={(e) => setUserName(e.target.value)}
				/>
				<br />
				<label>
					<p className="password">Password</p>
				</label>
				<input
					type="password"
					placeholder="Enter Password"
					name="password"
					className="login-input"
					required
					onChange={(e) => setPassword(e.target.value)}
				/>
				<br />
				<button type="submit" className="login-btn" onClick={handleSubmit}>
					LOGIN
				</button>
				<br /> <br />
				<span>Don't have an account?</span>&nbsp;
				<a href={routes.SIGNUP}>Sign Up</a>
			</div>
		</div>
	);
}
