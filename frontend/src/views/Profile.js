import React, { useEffect } from "react";
import user from "../api/user";
import useApi from "../hooks/useApi";
import "../assets/css/profile.css";

export default function Profile() {
	const getUserApi = useApi(user.get);

	const [userName, setUserName] = React.useState("");
	const [startDate, setStartDate] = React.useState("");
	const [endDate, setEndDate] = React.useState("");

	const MS_PER_DAY = 1000 * 60 * 60 * 24;

	const getUser = async () => {
		const { token, data } = JSON.parse(localStorage.getItem("user"));

		const response = await getUserApi.request({
			token,
			userName: data.userName,
		});

		if (response.ok) {
			setUserName(response.data.data.userName);
			setStartDate(response.data.data.license.startDate);
			setEndDate(response.data.data.license.endDate);
		}
	};

	useEffect(() => {
		getUser();
		// eslint-disable-next-line
	}, []);

	return (
		<div className="profile">
			<div className="card">
				<h1>Profile</h1>
				<p className="profile-user">
					Welcome! <u>{userName}</u>
				</p>
				<p>
					Subscription:
					<strong>
						&nbsp;
						{Math.abs((new Date(endDate) - new Date(startDate)) / MS_PER_DAY)}
						&nbsp;Days
					</strong>
				</p>
				<p>
					Start Date: <strong>{new Date(startDate).toDateString()}</strong>
				</p>
				<p>
					End Date: <strong>{new Date(startDate).toDateString()}</strong>
				</p>
			</div>
		</div>
	);
}
