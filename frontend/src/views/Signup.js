import React from "react";
import { useNavigate } from "react-router-dom";
import user from "../api/user";
import useApi from "../hooks/useApi";
import "../assets/css/form.css";
import { routes } from "../config/routes";

export default function Signup() {
	const navigate = useNavigate();
	const signupApi = useApi(user.signup);

	const [userName, setUserName] = React.useState("");
	const [password, setPassword] = React.useState("");
	const [startDate, setStartDate] = React.useState("");
	const [endDate, setEndDate] = React.useState("");

	const handleSubmit = async () => {
		if (!userName || !password || !startDate || !endDate)
			alert("Please fill in all fields");
		else {
			const response = await signupApi.request({
				userName,
				password,
				license: {
					startDate,
					endDate,
				},
			});

			if (response.ok) {
				alert("Signup Successful");
				navigate(routes.LOGIN);
			} else alert(`Error: ${response.data.message}!`);
		}
	};

	return (
		<div className="signup-container">
			<div className="signup">
				<label>
					<p className="user-name">Username</p>
				</label>
				<input
					type="text"
					placeholder="Enter User Name"
					name="user-name"
					className="signup-input"
					required
					onChange={(e) => setUserName(e.target.value)}
				/>
				<br />
				<label>
					<p className="password">Password</p>
				</label>
				<input
					type="password"
					placeholder="Enter Password"
					name="password"
					className="signup-input"
					required
					onChange={(e) => setPassword(e.target.value)}
				/>
				<br />
				<label>
					<p className="license-date">License Start Date</p>
				</label>
				<input
					type="date"
					name="start-date"
					required
					className="signup-input"
					onChange={(e) => setStartDate(e.target.value)}
				/>
				<br />
				<label>
					<p className="license-date">License End Date</p>
				</label>
				<input
					type="date"
					name="end-date"
					required
					className="signup-input"
					onChange={(e) => setEndDate(e.target.value)}
				/>
				<br />
				<button type="submit" className="signup-btn" onClick={handleSubmit}>
					SIGN UP
				</button>
				<br />
				<br />
				<span>Don't have an account?</span>&nbsp;
				<a href={routes.LOGIN}>Login</a>
			</div>
		</div>
	);
}
