import Login from "./views/Login";
import Profile from "./views/Profile";
import Signup from "./views/Signup";

import {
	BrowserRouter as Router,
	Navigate,
	Route,
	Routes,
} from "react-router-dom";
import { routes } from "./config/routes";

function App() {
	return (
		<Router>
			<Routes>
				<Route path={routes.LOGIN} element={<Login />} />
				<Route path={routes.SIGNUP} element={<Signup />} />
				<Route path={routes.PROFILE} element={<Profile />} />
				<Route path="*" element={<Navigate to={routes.LOGIN} />} exact />
			</Routes>
		</Router>
	);
}

export default App;
