const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.signup = (req, res) => {
	if ((!req.body.userName || !req.body.password, !req.body.license)) {
		return res.status(400).send({
			message: "All fields are required",
		});
	}

	const { userName, password, license } = req.body;
	User.findOne({ userName }).then((user) => {
		if (user) {
			return res.status(400).json({
				message: "User already exists",
			});
		}
		const newUser = new User({
			userName,
			password,
			license,
		});
		bcrypt.genSalt(10, (err, salt) => {
			if (err) throw err;
			bcrypt.hash(newUser.password, salt, (err, hash) => {
				if (err) throw err;
				newUser.password = hash;
				newUser
					.save()
					.then((user) => {
						res.status(200).json({
							message: "User created successfully",
							data: user,
						});
					})
					.catch(() => {
						res.status(400).json({
							message: "User creation failed",
						});
					});
			});
		});
	});
};

exports.login = (req, res) => {
	const { userName, password } = req.body;
	if (!userName || !password) {
		return res.status(400).json({
			message: "All fields are required",
		});
	}

	User.findOne({ userName }).then((user) => {
		if (!user) {
			return res.status(400).json({
				message: "User not found",
			});
		}
		bcrypt.compare(password, user.password).then((isMatch) => {
			if (!isMatch) {
				return res.status(400).json({
					message: "Invalid credentials",
				});
			}
			const payload = {
				id: user._id,
				userName: user.userName,
				license: user.license,
			};
			jwt.sign(
				payload,
				process.env.JWT_SECRET,
				{ expiresIn: 86400 },
				(err, token) => {
					if (err) throw err;
					res.status(200).json({
						message: "Login successful",
						token: token,
						data: user,
					});
				}
			);
		});
	});
};

exports.getUser = (req, res) => {
	const { userName } = req.body;
	if (!userName) {
		return res.status(400).json({
			message: "User name is required",
		});
	}

	User.findOne({ userName }).then((user) => {
		if (!user) {
			return res.status(400).json({
				message: "User not found",
			});
		}
		res.status(200).json({
			message: "User found",
			data: user,
		});
	});
};
