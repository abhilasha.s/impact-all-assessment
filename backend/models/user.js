const mongoose = require("mongoose");

const licenseSchema = new mongoose.Schema({
	startDate: {
		type: Date,
		required: true,
	},
	endDate: {
		type: Date,
		required: true,
	},
});

const userSchema = new mongoose.Schema({
	userName: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	license: licenseSchema,
});

module.exports = mongoose.model("user", userSchema, "user");
