const express = require("express");
const router = express();

const userController = require("../controllers/userController");
const checkAuth = require("../middleware/checkAuth");

router.get("/", (req, res) => {
	res.send("Hello World");
});

router.post("/api/v1/user/signup", userController.signup);
router.post("/api/v1/user/login", userController.login);
router.post("/api/v1/user/get", checkAuth, userController.getUser);

module.exports = router;
